# Flood

The Flood was a catastrophic event that killed thousands people,
displaces many more, and dramatically altered the course of history.

Modern calendar counts years since the flood (1 A.F. — "after flood")
and refers to dates before it as "before flood"<fn id="negative-dates">In the wiki
such dates may be shown as negative, year -30 = 30 B.F. That notation is not used in-universe.</fn>

Its cause is undetermined, but is believed to be natural rather than divine
(although religious groups that thought otherwise did exist).

Peoples of the Western Continent, who became ancestors of the modern <lore>Engwelian</lore> people,
had enough advance warning to prepare for an exodus in search for a new home.
Due to 
