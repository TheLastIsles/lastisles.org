# K'Tsonga

## Overview

The K'Tsonga people live on the North Isle, also known as the K'Tsonga Isle.
Most of them have a dark brown skin and dark curly hair.

K'Tsonga (_k'tson ga_) means "Sun land" in their language. Many non-native speakers
find the initial consonant cluster very hard to pronounce and say "songa" instead.
Many K'Tsonga find that amusing because "son ga" means "water land" instead,
and their isle is anything but — it's mostly a rocky desert and water is in short supply.

Life is concentrated in oases around water sources. Since there's so little arable land,
they have to cultivate it efficiently and use advanced irrigation systems and fertilizers.
It limits population growth as well. It's not surprising that it were K'Tsonga who invented
contraceptive potions that now make a popular export (and frequently stir controversies
in <lore>Engwelia</lore>).

Generally, most of their export is crafts and magical items such as potions and enchanted weapons.
In recent decades, discovery of large bat caves allowed export of bat guano — bird guano
has been used as a fertilizer for many centuries, but was considered too valuable to sell.

The most famous town of the K'Tsonga lands is <lore>Shidimba</lore> — literally,
a "knowledge town" or "knowledge oasis"<fn id="mba">Mba literally means oasis, but since all K'Tsonga
towns are in oases, the two are nearly synonymous to them.</fn>
It's the site of the oldest university in the post-<lore>Flood</lore> world.
Since that are was a highland untouched by the flood, the university library remained intact
and served as an important repository of pre-flood knowledge and cultural memory.

K'Tsonga society is highly individualistic and values intelectual pursuits and invention.
Tradition isn't a big part of it, and people who value tradition too much can be accused
of "clinging to the past". There's no hereditary nobility because individual achievment
is valued much more than origin. A darker side of it is that _lack_ of individual achievment
and skill is often treated with certain disdain and less capable people may not easily find
help they need.

Most insults in the K'Tsonga language have to do with target's perceived lack of intelligence
or skill. There are many ways to call someone dim-witted or clumsy, such as "twenty-toes"
(implying that someone's hand digits are no more dextrous than their toes).
Instead of masculine and feminine grammatical genders, it has animate and inanimate noun classes:
people, deities/spirits, and certain animals are animate; while everything else is inanimate.
The worst insult of all is to refer to a person in the inanimate "gender" and thus question
their very sentience rather than "just" intelligence.

K'Tsonga people have no family names since it can be seen as trying to appropriate
fame and achievments of your ancestors as your own. Many parents invent new names
for their children with an intention to make them sounds fresh and beautiful.
To speakers of other languages K'Tsonga ideas of phonetic beauty can be completely alien, though.
Conservely, words that Engwelian or Gaedelech people would consider beautiful tend to sound bland to
K'TSonga.

## Society and governance

Every oasis is an independent city-state governed by an elected council. Oasis councils act
as executive and judiciary government branches, but cannot make any new laws.

Laws can only be proposed and accepted by an assembly of representatives from all oases,
known as The Gathering. The Gathering is normally held yearly and coincides
with a big feast and fair, currently held on the day of autumn equinox
(the feast itself is, somewhat confusingly, named The Gathering,
and the assembly of council representatives is a part of it).

The Gathering also serves as an appeal court, and since it's the only appeal court,
any appeal can take up to a year.

Decisions are preferrably made by debate. If no agreement can be reached,
a decision is made by voting. To break a tie, one randomly chosen assembly member
is given two votes.

Technically, that member is considered a representative of a virtual member that is not present.
Sending a representative is a valid but heavily discouraged way to participate in The Gathering:
if an oasis is dealing with some extraordinary circumstances, its council can ask a representative
of another oasis to act on their behalf.

## Foreign relations

Engwelian merchants were trading with K'Tsonga for centuries, but about a century ago
a permanent Engwelian mission was established in Shidimba. Misunderstandings and confusion
are still common: Engwelian diplomats have a difficulty coming to terms with the fact
that each oasis can have its own foreign relations and that there's no "K'Tsonga king"
who would hold the ultimate authority.

Despite those difficulties, there are significant diplomatic achievments,
such as the acceptance of the common <lore>criminal law</lore>
that put an end to Engwelian criminals trying to hide from justice in K'Tsonga lands
and vice versa. Political and religious dissidents escaping to K'Tsonga lands
remain common and sometimes become a source of diplomatic tensions.

## Economy

As said above, the main export of K'Tsonga oases is crafts and magical products
such as potions and enchanted weapons.

The main import is staple food and timber. Since farming in the limited space
of oases and the arid climate of the island is challenging, most oases now prefer
to buy staple food from Engwelia and focus on growing tropical fruits
and medicinal plants that don't grow anywhere else.

Around 370 AF, the discovery of large deposits of bat guano in caves around the coast
allowed them to start exporting it to Engwelia
