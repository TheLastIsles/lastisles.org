
# Gaedelech language

The language of the <lore>Gaedelech</lore> people was properly called "awrdchell" ("native speech").

It rapidly lost its social prestige and quickly became a dead language when the ancestors of modern
<lore page="Engwelian">Engwelians</lore> that escaped the <lore>Flood</lore> colonized the lands
of the Gaedelech people and assimilated them.

In modern times, only a small number of scholars can read it and there are no fluent speakers.
Some people with Gaedelech ancestry are making attempts to reclaim their identity and revive the language, but their movement
is yet to gain any momentum.

Gramatically, it's a synthetic, highly agglutinative language.

A curious feature of the language is that many words are somewhat ambiguous. For example, in <place>Lenornsedfarch</place>
(len-orn-sed-farch — most-dark-deep-cave), "len" is a superlative suffix, so it could mean "the darkest cave" or "the deepest cave",
but it's not known which adjective it applies to. "Chellarn" (speech/word-person) could mean (usually) a poet or writer,
or simply someone who talks too much.

Sometimes ambiguous words shed light on the Gaedlech culture. "Ghestarn" literally means "profit-person" and had two meanings in Awrdchell:
a merchant, or a selfish person who only thinks about personal gain. Indeed, professional merchants were viewed with a certain degree of suspicion
and disdain and there apparently wasn't a truly neutral word for that occupation.

Most of the time if two nouns are strung together, the first one is the determinant, but there are known exceptions.
For example, "taifarn" (house-defender) means fortress, a "defended house".

It's not known if there were specific rules for resolving those ambiguties that are now forgotten,
or they used to rely on context and tradition to infer the intended meaning.

## Phonology

Vowels:

* a — always _ah_, except in the 'ae' diphtong.
* e — always _eh_.
* i — _ee_ (or _y_, when terminal).
* o — always _oh_
* u — (always _oo_
* w — short 'oo' after vowels.

Diphtongs: ae (_ei_).

Consonants: 'c' (always 'k'), d/t, f, h, gh/ch, r, n, m, l, s, w, sh.

Pronunciation:

* r — voiced velar approximant, like in rhotic dialects of English (not a trill like in Spanish).
* gh — voiced velar fricative, like 'г' in Ukrainian (usually pronounced like 'g' in 'good' by non-native speakers).
* ch — voiceless velar fricative (like in Scottish "loch").
* h — glotal stop or voiceless glottal fricative.
* w before vowels — voiced labial-velar approximant (like in English).
* ll is an allophone of l.

## Pronouns

* mi — first person singular.
* aw, or its archaic form "awrd" — first-person plural (we, us, ours).
* te — second-person singular.
* wois — second-person plural.
* ish — third-person, gender-neutral (it/she/he/they).

## Nouns

* ghirn (mountain)
* farch (cave, cavern)
* gaed (highland)
* conn (field)
* cem (tree)
* arn (human)
* lech (tribe)
* tai (home, place of habitation)
* shourn (the Sun)
* chell (language, speech)
* farn (defender, guard, warrior)
* woh (axe)
* ghan (sword)
* dins (song)
* morin (stone, rock)

"Arn" is often used as a suffix for occupations or habits. Examples: "dins-arn" — "song-person", a bard; "chell-arn" — "speech-person"
(a speaker or a writer, or someone who talks too much, depending on context);
"conn-arn" — "field-person" (a farmer), "tai-arn" — "home-person" (homemaker, or a person who spends a lot of time indoors).

## Adjectives

* orn — black, dark
* cas — red
* sho — white
* sed — deep
* len — great, prominent, important
* cwon — foreign, alien
* argh — dead
* owc — intense, fierce
* tarl — raw, rough

Any adjective can be used as a prefix. Lenarn — a very important human (chieftain, leader), lenghirn — a very high mountain, lenfarch — a very deep cave.

Adjectives are often combined with nouns in a single word: "dark cave" can be written either "orn farch" or "ornfarch".

Any noun can be used as an "adjective prefix". 

"Gaed-elech" — a highland tribe, "cwon-lech" — a foreign tribe. "Argh-tai" (dead-house) — "home of the dead" — grave, tomb.
"Owc-arn" ("intense-human") — an impressive person, a master of some skill. 

It's not clear how superlatives were formed. <place>Shourntai</place> ("home of the Sun"), the highest mountain in their lands, was referred to as "lenghirn"
("great mountain"), but it's not clear whether "len" was reserved only for the most prominent objects of their kind, or any high mountain could be called "lenghirn".


## Suffixes

* en — plural.

Cemen — trees, arnen — people.

* rech — a suffix of collective and abstract nouns

Cemrech — a forest, arnrech — a crowd or humankind, ghirnrech — a mountain range.

* clas — a suffix of abstract nouns with a "container" connotation.

Taiclas — "something that contains houses" — a (walled) city. Compare and contrast with "tairech" — "a set of houses" — a village.
"Lentaiclas" — most important place that contains houses — a capital. "Ghanclas" — "a container for swords" — a sheath.

## Verbs and tenses

Most nouns and adjectives are made into verbs with "-anorn" suffix.
"Woh-anorn" — "to axe (chop)", "orn-anorn" — to blacken, "chell-anorn" — to speak.

There are no progressive aspects, so e.g. present simple and present progressive
can only be distinguished from context, if at all.

It's possible to form verbs that aren't found in authentic Awrdchell texts
but could have valid meanings: "taianorn" ("to home"), "cwonanorn" ("to become alien").

Any verb can technically be used as either transitive or intransitive.

Tenses are expressed using different suffixes:

* -anorn (present simple or present continuous, also infinitive)
* -anern (past simple)
* -anirn (future)

Perfective aspects are formed with an additional "-t" suffix. 
"Aw chell-anorn" — "we speak/are speaking", "aw chell-anern-t" — we have spoken.

Note: perfective suffixes are sometimes used in the present tense to mean
an ongoing attempt to complete an action.
"Aw chell-anorn-t" — "I'm trying to say (something)".

The "rech" suffix can be added to make an action or state noun: "tai-farn-anorn-rech" — "house defense"
(the process or the art of defending one's house).

The "och" suffix roughly means "is being". "Cem wohanornoch" — "the wood is being chopped".
 
### Verb mood markers

* Imperative: "-di". "Chell-anorn-di" — "speak!".
* Potential: "-wa". "Chell-anorn-wa" — "you may speak".
* Optative: "-wad". "Chell-anorn-wad" — "I hope you speak".

Mood markers can be used with any tense. "Aw chell-anorn-wad-et" — "I wish we had talked".

## Sentences

The usual order is SOV. The object is often incorporated in the verb.

"Aw cem-woh-anorn" — "we wood-axe"/"we are wood-axing" (i.e. "we are chopping wood").
"Aw farn-anorn" — "we are defending (ourselves or something)".

"Tai farn-och-anorn" — "the house is (being) defended/guarded".

## Negation

Negative verbs are formed using an additional "-igh" suffix.

"Chell-anorn-igh" — "to not speak" (to keep silence). "Chell-anorn-igh-di!" — "be silent!".

However, "is not" meaning is conveyed with a negative copula "ghil".

"Mi dinsarn ghil" — "I'm not a bard" (literally, "I bard am-not").

## Trivia

The phonology of Awrdchell is designed to sound vaguely Celtic,
but its grammar is inspired by Turkic languages. 
