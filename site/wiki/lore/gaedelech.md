# Gaedelech

The Gaedelech people were the original inhabitants of the land now known as the <place>Last Isles</place>.
They spoke the <lore>Gaedelech language</lore>, properly called Awrdchell.

The influx of settlers from the continent who came to the isles to escape the <lore>Flood</lore>
disrupted their societal structure. The ancestors of modern-day <lore page="Engwelian">Engwelians</lore>
viewed their culture as privimive and saw themselves as bringing civilization to their land.
Due to their superior technology and military tactics, early Engwelians had little difficulty claiming
more and more land for themselves and displacing the natives.

Over time, many of the Gaedelech started to see joining the Engwelian society as an attractive option.
Their language rapidly lost its social prestige and two centuries after the flood
there were no native speakers of it anymore.

In modern times, the highest population of people with Gaedelech heritage is found in the county of
<place>Casmorintai</place> on the <place>North Isle</place>.
