# Engwelian names

## Given names

### Unisex names

Yerrine, 

### Female names

Tirra, 

### Male names

Rinno, Engwel, Anno

## Family names

### Noble families

* Anniutto
* Vernio
* Eweldo
* Qwode
* 

### Commoner families

Commoner family names are usually derived from occupations. They normally end with an "O"
and stay the same regardless of person's gender.

* Farro (blacksmith)
* Aurerro (goldsmith, jeweller)
* Fornio (baker or stove builder/repairer)
* Camnio (chimney sweep)
* Carbio (coal burner or miner)
* Botro (grape grower or wine maker)

Family names can also be derived from first names using suffixes "-isa" ("daughter") or "-ise" ("son").
If a name ends with a vowel, that vowel is removed. For example, in a family of descendents of someone named Yerrine,
a female member may be named _Tirra Yerrinisa_, while a male member may be _Anno Yerrinise_.
