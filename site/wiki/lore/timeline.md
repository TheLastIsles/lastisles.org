# Timeline

## Before the Flood

* -1 BF — sea level starts rapidly rising.

## After the Flood

* 1 AF — the ancestors of modern <lore>Engwelian</lore> people leave the <lore>Western continent</lore> in search of new places to settle.
* 30 AF — <lore>Engwel the Great</lore>, the noble who led the exodus from the old continent, leaves for an expedition to find remaining land and is never heard from again.
* 107 AF — Yerrine the Traveller makes contact with the <lore>Ushel</lore> people and brings back many innovations, including south-pointing needles and cures for previously incurable diseases.
* 110-113 AF — the Ushel disappear from their <place page="Ushel isle">island</place> without a trace.
* 
* 199 AF — a caravan led by a famous adventurer named <lore>Mbunwo</lore> disappears in the desert, and <item>Wanderer's robe</item> is lost with it.
* * <lore>Rinno the Second</lore> abdicates and leaves his throne to his son, <lore>Rinno the Third</lore>. 
* 300 AF — the <lore>Dragon Tooth dagger</lore> is found to be missing from the Engwelian treasury.
* 326 AF — the king of Engwelia hires a survivor of a shipwreck to search for the Dragon tooth.

