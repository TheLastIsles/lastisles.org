# Ushel

The Ushel were the most mysterious people of the post-<lore>Flood</lore> world.
They lived on a small island far to the north of the <place>Ktsonga isle</place>.
It's unknown when and how they came to that island, now known as <place>Ushel isle</place>.

They left their isle sometime between 110 and 113 A.F.
