# Last Isles

The Last Isles is a group of four large and numerous small isles located in the Southern Hemisphere of the <lore>world</lore>.
It's the home of the peoples who appear in the game and the only habitable land known to them after the <lore>Flood</lore>,
hence the name.

The two southern isles were isles before the Flood and were originally ihabited by the <lore>Gaedelech</lore> people.

The other isles are remnants of a continent.
