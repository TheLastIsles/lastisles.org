# Engwelian

Engwelians are successors of the people from the old continent who escaped the <lore>Flood</lore>
on ships led by <lore>Engwel the Great</lore> and settled on the <place>Last Isles</place>.

Since their ancestors came from multiple different nations, their appearence can be quite diverse:
from dark-haired and brown or olive-skinned to blond and pale-skinned and everything in between.

Their <lore page="Engwelian language">language</lore> is derived from the language of a vast empire that once existed on the old continent
and served as a _lingua franca_ of that region after old empire's breakdown.
Nobles and educated city dwellers speak a form of that language that is close to the original
language of the old empire, known as "High Engwelian"; while lower classes speak a simplified version
known as "Low Engwelian". The upper classes see the "Low Engwelian" as primitive and corrupted.

