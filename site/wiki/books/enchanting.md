# Beginner's guide to enchanting

Enchanting, the art of creating magical items, has been known from the ancient times,
but it made a dramatic progress in the last couple of centuries.

The reason it's been known from time immemorial—from the very time people first learnt to use magic,
is that conceptually it's a very simple process.

All it takes is to put a magic stone in contact with the item to be enchanted, break its crust,
and cast a spell you want to impart.

The current theory is that the magical energy stored in the stone is transferred to the item,
while the spell changes it so that it starts casting the spell on everything it comes in contact with.

Of course, if it was _that_ simple, we all would be great enchanters already. For a long time,
mages didn't understand why enchanting the same item with a stone of the same size can result
in items of vastly different power. The fact that enchanters and mages of the old days in general
didn't like to share their techniques. Tireless work of the Guild of Mages revealed the truth:
the key is timing. The spell should be cast exactly at the moment when the crust of the stone
is broken and its magical energy is starting to transfer to the item.

That discovery is the reason why powerful magical items are now available to anyone who can afford them,
and best enchanetments already rival those of legendary artifacts. However, it takes a long time
to achieve perfect timing required for a professional-level enchantment.

Enchanters from the Guild are trained for three to five years, on stones and items provided by the guild,
and then they have to spend ten years working for the guild while the guild will take most of their earnings.

But of course not everyone wants to reach an advanced level, and many people dabble in enchanting
as a hobby. If you want to try your hand at it, this guide is for you.

## Choosing an item to enchant

Not all materials are receptive to enchanting. Metals work best, which is why many enchanted items
are jewellery and weapons.

Gemstones also can hold enchantment well. Wood, leather, and any other animal or plant material
isn't receptive at all. That's what allows enchanted weapons to be safely sheathed without the spell
affecting the sheath. Paper used for enchanted scrolls isn't ordinary paper, but paper with thin foil glues to it.

## Obtaining magic stones

Magic stones are literally everywhere, but most of them are too small to be useful, and are only good for practice.
They are never found in formations and seem to be evenly scattered around the world.
If you dig in the mud long enough, you will surely fine some. In fact, many farmers make some pocket money
selling stones they find in their fields.

Magic stones are easy to recognize. They are much lighter than any other stones found in nature.
Despite their magical nature, their look is unremarkable: they are covered by a light gray crust,
but once you've seen one, you will know what they look like.

No one knows how they formed, and their origin is a mystery. Some claim that they are pieces
of the luninous aether itself, somehow solidified and protected by a firm crust.
That theory is appealing because in that case there may be a way to make new stones artificially
by condensing the aether, but no one demonstrated that yet.

Without a way to condense the aether, we have to rely on blind luck. Magic stones are usually
divided into three grades: fingertip, thumb, and first.

Fingertip-sized stones are mostly good for practice. Thumb-sized ones are much more valuable,
and fist-sized ones are so rare that any such find quickly becomes a sensation.

A good place to look for stones is riverbeds. The sea floor may hide true treasures,
but it's difficult to explore, being deep under water. If you simply want to dabble in enchanting,
your best bet is to simply buy a bag of small stones from an enchanter.



