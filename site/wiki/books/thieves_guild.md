# Thieves company

So, for the sake of argument, let's say you decided that none of the trades
your parents and teachers suggested look appealing to you. You don't want to be a baker,
a healer, a warrior, even the trade of an adventurer and treasure hunter isn't for you.

You want to capitalize on people's inattention and disregard for the security of their own
dwellings and posessions. You want to be a thief.

Becoming a thief is very easy. Go to the market square and grab something from the nearest vendor's stand.
When the vendor starts calling guards, you are officially a thief, enjoy your new title.
Wait, you mean you want to be a thief and remain a citizen in good standing?

That's somewhat harder, but doable. I'll tell you how. Wait, who am I to teach you?
Well, I'm the ace of the worshipful company of crooks and thieves.
The ace? Yes, our ranks are named after playing card ranks. You start as a jack, then you can be
promoted to a queen, a king, and if you are really cunning and lucky, get elected as the ace.
No, the ranks aren't adjusted for gender. Half the kings are women and I used to be a queen, long ago.
We don't attach a special significance to it.

Oh, speaking of cards and dice. Don't bet money you don't have. You will lose respect of your peers,
and respect is the most valuable currency in our trade. Many people dug themselves deeper
by going on a job without proper preparation, driven by desire to pay off their gambling debts, and ended up in jail.
Members with outstanding debts are excluded from the mutual aid until they repay the debt,
so their jail time is worse than it could be if they were wiser.

For members in good standing, we have a mutual aid fund, training, safe houses, and other nice things.
You should join us. Of course I'm biased here: I get a few coins from each member's dues,
so you will make me slightly richer if you join. But I promise you it's worth it.

What, you think I can't be called a thief if all I do is collecting dues?
Stop these baseless accusations, I can steal better than most. I pick pockets in broad daylight
for my own amusement—and for profit of course. But I also do quite a lot of diplomacy,
including communication with guards. You can't be the ace simply by being a good thief,
my job is to make sure the guild is operational and safe first of all.

I'm sure you are already thinking, "communication with whom, GUARDS?!". Well, yes. The guards.
No, we don't tolerate spies, and the guards don't know everything we are doing—they don't know much in fact.
However, an unwritten contract with the guards is what allows us to exist without going too deep underground.
They tolerate us because we make the underworld better, and because they get a way to avoid a big scandal
if somethng is stolen from an important person.

What's the contract? Pretty simple. First, our members must not harm their marks. Material posessions are fair game, but
life, limb, and dignity of the mark are not. In other words, pickpocketing and breaking into buildings are acceptable
ways to make a living, armed robbery and murder are not. If you break this rule, everyone will hate you:
your peers, guards, and the commoners alike. This rule is the reason why we are known as thieves with moral principles,
not as dangerous and unpredictable bandits. Sometimes we even help guards track down violent criminals.
Theft is the only fully reversible crime, which is why it's the least hated one.

Confidence tricks are in a gray area—we don't punish members for that, but it's a bit too visible,
and we prefer not to attract attention.

Second, don't steal identifiable items from people in the positions of power. Stealing a crown from a duke would be a rather
dumb idea anyway, don't you think?
If you do, we'll have to return it to the owner with an apology. I know it may feel like selling out, but that's how we remain on the loose
more time than we spend in jail. In return, guards turn a blind eye to the existence of trusted people
among them who will expunge criminal records for a fee, help us take contraband to jails and penal colonies,
and help us get more lenient sentences in court.

Of course, there's also an honor code. Don't ruin your reputation with people—there are many people among the poor
and even among the middle class who admire and support us, and it's better to keep it that way.
Stealing from the rich to give to the poor is not necessary, but at least don't steal from respected and honest
merchants and people who make their living through hard work. If you are caught, no one will think twice before
reporting you to the guards, but if you pick someone people don't like as your mark, people may even help you
get away with it.

Some beginners actually start out stealing from the poor because their houses are easy targets.
Don't do that. We have a fund to help the poor and we make anonymous donations—everyone easily guesses
who the benefactor is, and that's another reason people us—or at least don't hate us too much.

So, you want to join? The best way is to know someone who's already a member and can introduce you.
One place to meet members is, well, a jail. If you don't want to go to jail, you still can find us.
Here's a hint: look for a place you'd never think can be a front for the company of crooks and thieves.
Good luck.

