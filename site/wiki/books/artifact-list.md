# List of artifact locations

WARNING: THIS DOCUMENT IS CONFIDENTIAL!
Any copying and dissemination must be approved by the museum custodian or the university rector. 

## Held by scholarly societies and museums

### Robe of Ozek

The Robe of Ozek is held by the University's alchemy workshop and used for researching
poisons and protection from them.

## Privately owned

## Lost

These artifacts are known to exist but were stolen or lost and their current locations
aren't known. 

### Flaming Sword

The Flaming Sword was lost in a failed mission to cleanse the Eagle's Nest castle
from demons. If found, it should be returned to the Engwelian Temple.

### Wanderers's Robe

The Wanderer's Robe was last owned by the renowned adventurer Mbunwo about a century ago.
He was guarding a caravan that disappeared without a trace.

The Museum is ready to purchase it from anyone who finds it.

### Wisdom Ring

The Wisdom Ring was left in the old Temple of Vianna, now located deep underwater.
Its retrieval is considered an intractable problem, but the Museum is ready to purchase
it from anyone who succeeds at it.

## Legendary

### Bird Helm

The Bird Helm is considered merely a folk tale, but if anyone proves otherwise,
the Museum may be interested in acquiring it.
