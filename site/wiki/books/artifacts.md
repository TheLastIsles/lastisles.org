# Artifacts and relics

### Flaming Sword

The Flaming Sword is an artifact associated with Engwelian crusaders
that was created by an unknown priest to help them fight demons.
The sword burns its target with magical fire.

It's held by the Engwelian Temple, but currently isn't on display.

### Dragon Tooth

The Dragon Tooth is the most important symbol of the royal power
and the unity of the Engwelian kingdom.

It's a tooth of a dragon that menaced the settlers of the isles that belong
to Engwelia now. Engwel the Great defeated him and made his tooth into a dagger.

It's used in the coronation ceremony, but the nature of its power is kept secret.

A replica is currently on display in the Royal Museum of Arnen, the capital city
of Engwelia.

### Robe of Ozek

The Robe of Ozek is an enchanted cloak with a hood that protects the bearer from
poison. it's origin is uncertain: some claim it was made by an alchemist named Ozek,
while others think its name means something in a long-forgotten language.

It's currently held by the Oasis University's alchemy workshop and used for researching
poisons and protection from them.

## Lost artifacts

### Wanderer's Robe

The Wanderer's Robe is an enchanted cloak that grants its bearer protection
from elements: heat, frost, and even lightning.

Its origin is unknown, but it was owned by numerous adventurers
over the centuries, with earliest records of it going back to
thousands years before the flood.

It was last owned by the famous K'Tsonga explorer Mbunwo about a century ago,
and was lost when his caravan disappeared without a trace.

### Wisdom Ring

The Wisdom Ring, as its name suggests, gives its wearer increased intelligence.
For that reason, it's sought after by scholars and wizards.
It looks like a plain gold ring with an inscription that reads "this too shall pass".

It was comissioned by an ancient king who wanted to be a wise ruler of his lands.
Since then it was lost and rediscovered many times.
In the last centuries before the flood, it was traditionally worn
by the high priests of Vianna—quite fitting for a cult of the goddess of healing and wisdom.

Unfortunately, when the flood reached her old temple, in the chaos that ensued, the ring was
left inside. Since it's made from gold and salt water can't damage it, it's probably still intact,
but retrieving it from such a great depth would be an incredibly challenging task.

### Bird Helm

The Bird Helm is the most unusual artifact. Wearing it is a curse rather than a blessing:
it makes its bearer unlucky, in addition to looking ridiculous—it looks like a bird sitting on bearer's head.
Worse yet, it cannot be removed by any normal means.

According to a legend, a warrior asked a wizard for an item that would allow him to show that
he's the greatest warrior of all. The wizard made that helmet as a way to teach that warrior humility.
However, the warrior kept fighting battles and winning them despite the cursed helmet,
and many praised him as the greatest warrior of his time for that.

The ultimate fate of the warrior and his cursed helm are unknown.
Since it cannot be taken off, anyone who finds it should handle it with caution and never try to wear it.
