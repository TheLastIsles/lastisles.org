# Engwelian Nobility

by Dzidimbo

One of the most prominent and pervasive elements of Engwelian society
is the concept of "nobility" and the dichotomy between nobles and commoners.

Nobility is an elevated status in society that can be acquired by birth or marriage
or granted by the king. It grants special rights and privileges to its holders, such as:

* right to own land and profit from it;
* right to attend events and enter buildings reserved for nobles;
* right to audience with the king and local rulers.

There are more privileges that are not mentioned in the laws but exist in practice,
such as milder punishments for many crimes.

There are following nobility titles: king, duke, count, baron, and knight.
Female nobles historically had different titles (queen, duchess, countess,
baroness, and dame), but those distinctions are falling into disuse.

The king rules over the entire land, dukes and counts rule their own counties,
while lesser nobles such as barons and knights can own land but
don't have a sovereign rule over it.

Marriage to a noble of a higher status elevates one to that status.
For example, if a knight marries a baroness, he becomes a baron
and receives privileges of that title.

Marriages between nobles and commoners are also increasingly common,
As the number of wealthy commoners is growing while the wealth of many lesser nobles is dwindling,
such marriages often help nobles to stay afloat, even though it's often frowned upon
and considered a sale of nobility.

Counts and dukes are able to maintain their wealth since peasants are required to sell their produce
to their rulers and cannot directly trade with anyone from other counties or overseas.

In a notable and mildly controversial case, the king granted nobility to a winemaker
who made his favorite wine, and whose ruler was taking advantage of that law.
That count kept buying wine from his subject at the same price but raised the sale price
for the king every year, and eventually the king made that winemaker a baron — formally
to acknowledge his achievements in wine making, practically — to give him a right
to sell his wine directly to the royal court.

Commoners, no matter how wealthy and prominent, are prohibited from attending nobles' social gatherings.
However, nobles are seen attending social gatherings of wealthy commoners more and more often.

All in all, while nobility is still an important part of the Engwelian society,
it's certainly changing and its future is far from certain and the influence of wealthy commoners
is growing faster than many nobles are willing to admit.
