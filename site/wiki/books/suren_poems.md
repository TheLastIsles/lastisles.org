# Selected poems by Suren

[This is a selection of Suren's authentic poems made possible by the discovery
of a damaged but readable pre-Flood print edition in the archives
of the Royal Library]

## Like autumn leaves...

Like autumn leaves, they fall upon my heart,
your parting words, your solemn farewell.
How long will our fate keep us apart
And will we meet again, how can I tell?

These shores are not for me, I fear, my friend.
No choice but to leave; I cannot stay.
This tearful meeting soon will have to end
And salty waves will carry me away.

Who knows if I will ever see this land
become a land that welcomes me again.
Who knows if I can step on its shore's sand
And feel the air of freedom soothe my pain.

Now, like an autumn leaf, I'm blown away
But I still hope that I'll return someday.

