# Introduction to spellcraft

Magic is everything that involves manipulating forces that are invisible and aren't truly understood.
Of all kinds of magic, spell casting is the most direct, even if the least understood way to manipulate them.

A spell is a sequence of sounds that produces a magical effect. No one knows how it works.
The most common theory is that there's a certain kind of energy that permeates the universe,
and spell sounds somehow disturb, transform and focus that energy.

It's presumed that people learned the first spells in the ancient times, by imitating the noises of animals that are capable
of magical attacks. Indeed, many sounds used in spells have nothing to do with the human language,
and sometimes major breakthroughs were made by discovering sounds that were thought to be impossible for a human to make.

It's also one of the major challenges for beginner spellcasters who have to memorize sequences of seemingly random noises
and reproduce them correctly. Every detail counts, including the pitch of your voice and whether the pitch is rising or falling.

That also makes spells impossible to write down, which greatly hinders their research. Spells are only taught person to person,
and reliable notation for them is an open problem.

It's still not known how to construct a spell with desired effects. There are people who can do it, but it takes some experimentation
even for them, and it's more of an art than a science. However, it's observed that different schools of magic have different sets of sounds
in their spells.

## Schools of magic

Magic is divided into three schools, depending on the effects it has on its target.

White magic spells have a positive effect on the body: cure diseases, heal wounds, grant a temporary increase in strength,
endurance, or intelligence, and so on. The Temple priests are unchallenged experts in that school.

Gray magic has spells that can be used for either good or evil. What they have in common is that they manipulate space
and natural forces. There are spells for instantly moving over a large distance, becoming invisible, manipulating remote objects,
moving through walls, and so on. Those effects are equally useful for traveling and for thievery.

Some forms of gray magic, such as spells that instantly move you to a temple, as widely available. Other spells, like invisibility,
are looked down upon by the Temple and the guards, and are much harder to obtain.
Some talented practitioners of gray magic appear to be capable of shapeshifting and creating elaborate illusions.
A few decades back, traveling performers presented a show of "magic fully exposed" in the capital, and did quite some mischief,
from making people take wine bottle corks for coins to instantly sending the theater director across the country.
They were never caught, and their feats still haven't been replicated, but it shows how powerful gray magic spells can be.

Black magic has destructive spells that injure their targets with fire or cold or drain the very life force from a target.
It's widely used by mages for self-defense, and Temple's crusaders dedicate a lot of time to its study so that they can fight
cultists of the dark gods and demons they summon.

Finally, there are spells that fall under "forbidden magic". It's not a single school, but rather a collection of spells
that are thought to be so vicious and immoral to use that no reputable mage would use it.
There are spells for rising undead minions and controlling living people, for example.
There's also demon summoning, though it's still not clear whether it's really a spell, or rather a power granted by the dark gods themselves.
