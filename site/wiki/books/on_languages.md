# On languages

The Engwelian language essentially exists in two forms now: the high language spoken
by the learned people; and the low language spoken by commoners.
It is, however, wrong to think that the low, or folk Engwelian is a corrupted form
of the high language.

Both these languages descended from the language of the Old Empire.
When the Old Empire broke down, its original language also started breaking down
and gave rise to national languages of various peoples, but its original form was still
spoken as a common language in the ports and markets of the Old Continent, albeit
in a simplified form.

The original, unadulterated language of the Old Empire also remained in use
long after its breakdown, as an auxiliary language of diplomacy, religion, and
science.

When our ancestors fled the Old Continent to escape from the Flood,
those old languages switched from their auxiliary roles to primary languages,
since Flood survivors from various nations needed to communicate,
and none of the groups were numerous enough to force their national language
on everyone else. Naturally, they turned to languages that everyone had at least
a rudimentary command of: one variant of the Old Empire's language or another.

National languages definitely had an influence on the new common language,
and many words of ancient origin were replaced by loanwords.
Still, its grammar decayed much earlier, and its grammatical simplicity
is what the learned people usually see as its most offensive quality.

For example, most commoners do not make a distinction between
"I write" and "I am writing", or between "I spoke" and "I have spoken".
That is, however, not a recent development but its original deficiency:
folk songs that usually pass between generations unchanged confirm
that those distinctions were lost on the illiterate folk for centuries already,
before the Flood, and possibly even earlier.

However, as primitive as it is, even the simplistic folk Engwelian sounds precise and elegant
compared to the language of the original inhabitants of the Last Isles, the Gaedelech people.
