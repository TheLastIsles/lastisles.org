# Alchemy for beginners

When people ask me which magical skill they should learn first,
my answer is always the same: alchemy.

Sure, throwing fireballs, becoming invisible, or instantly moving through space
is more impressive and, let's face it, more fun.
However, all those things also require a lot of dedication and practice to do _reliably_.
And until they are reliable, they are nearly useless in the real world.

There's nothing more miserable than a person who wandered into wilderness
thinking they can always teleport back to safety, only to repeatedly fail the spell
and get exhausted.
What you can do in class under your teacher's guidance is much harder to do
by yourself when you are tired or in danger.

But potions, once you have brewed them, will be good for a long time
and you can keep them in your bag until they are needed,
and they will give you the effect you need.

There are two phases of potion making: extraction and distillation.
Extraction pulls the magical essense out of your ingredients.
Usually it's done by grinding ingredients in a mortar and boiling them.

In an emergency, you can make a potion simply by grinding and mixing its ingredients together,
but such potions will not be very strong.
However, it can be done even in the wilderness, since you only need a mortar and pestle to do that.

If you have access to a proper alchemy lab, there are more options for making more efficient
potions and controlling their effects.

To make your potion stronger, you need to distill it using an alembic.
An alembic consists of two vessels connected by a pipe: one of them is heated
so that the potion evaporates, the other collects the vapors.

Distilling a potion increases its strength, but also decreases its duration.

Some potions don't make any sense to distill at all. Water breathing, for example.
You can't breath under water "better" or "worse": you either can do it, or you cannot,
the only question is for how long.
Distilling a potion of water breathing will only decrease its effect duration.

In other cases, it's a compromise. You may want a potent potion that increases your strength
to get through a fight, in that case you can sacrifice its effect duration for potency.
Or you may need to carry a heavy load over a long distance, then you need your strength boost
to last longer.

Now go gather some ingredients, get yourself an alchemy set, and start practicing.
