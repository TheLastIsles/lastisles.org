# Ushels and their legacy

The Ushel people remain one of the most mysterious nations.
Our history of contacts with them was very brief, we never really knew them, yet
many of the great inventions were learnt from them,
including south-pointing needles, long division, and the game of stones.

No one is certain where they came from and where they have gone.
They inhabited a small island that was too remote for merhcant ships of the time to reach and too
infertile for farming.

They were secretive, but not hostile. Few people dared to go to their island,
but those who went there and returned told they weren't threatened.
The Ushels seemed to understand their language, but they never talked to anyone.

Our most detailed account, however, comes from just before they sailed away.
Yerrine	the Traveller ventured to their	island specially to contact them,
and to his utmost surprise, they spoke to him and took him to their village.

There were some	plain wooden houses, but most Ushels lived in an underground
structure he wasn't allowed to enter.
They questioned him and tested his knowledge to confirm he is indeed a scholar.
Their command of the Engwelian language was pretty good though they spoke with a strange accent.

They had no farms or water sources on the surface, but the underground structure was presumably
expansive. It's not known how numerous the Ushel people were. He only saw perhaps a dozen people
ever go to the surface.
They refused to tell anything about themselves, and only told him that "Ushel" isn't their real
name, but that's how they want to be known.

They spoke an incomprehensible language.
Only a few words from the Ushel language are remembered, and their meanings are uncertain.
Among them "reetag", "heptil", and "satlit".
When he asked, they told him their meanings would be completely unfathomable for us even
with explanations.

Their chieftain was called Zavlab, though it's not known whether it was her name or title.

According to his report, Zavlab treated him courteously. She spent a few days teaching
him how to make south-pointing needles, how to treat some diseases thought to be incurable,
and many other things.

He dedicated most of his life to spreading the knowledge of those inventions.

When he attempted a second expedition three years later, he found the island empty.
It's not known where they had gone or why, and it will forever remain a mystery.
