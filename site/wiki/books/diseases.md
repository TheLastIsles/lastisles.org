# Relief of Fevers

Thanks to the work of healers and alchemists, all known diseases are curable now.

They can be cured with spells or potions. However, you need to use a spell or potion
of appropriate strength. Common cold can be cured with a weakest and cheapest potion,
while serious diseases need potent potions or spells.

Cold
  Common cold is a mild disease. Its typical signs are runny nose, fever, sneezing and coughing.
  While rarely if ever fatal, this disease can still affect every aspect of your abilities.

  Common cold usually resolves within a week and doesn't require treatment.
  Most healers consider treating it a waste of potions.

  One school of thought maintains that you can catch cold by failing to stay warm in a cold weather,
  but some claim that sick people spread the disease by sneezing and coughing, and being cold
  only makes you more vulnerable to it.
  The most daring claim is that cold weather simply hastens its spread by keeping people indoors
  in a close contact.
  Until that dispute is resolved, the most sensible advice is to avoid being cold and stay away from sick people,
  though this advice can be difficult to follow.

Relapsing Fever
  Intermittent fever is thought to be insect-borne. Its victims suffer bouts of fever and rash
  that usually last a few days. Some people may suffer a bout of fever every week,
  others may remain well for many weeks and suddenly fall ill again.

  Due to its unpredictable nature, everyone should seek treatment.

Red Fever
  Red fever is a serious disease that causes spontaneous internal and external bleeding.
  It's invariably fatal without treatment and must be cured as soon as possible.

  Red fever is spread by wild beasts. 

Heavy chain disease
  Heavy chain disease is a serious disease that makes its victims feel weak,
  as if bound by a heavy chain.
  Without treatment, the afflicted keep losing their strength, endurance, and health
  and eventually die.

  
