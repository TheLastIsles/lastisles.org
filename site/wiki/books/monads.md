# A mysterious scripture

[This scripture is thought to originate from the [Church of the Least Fixed Point](https://wiki.c2.com/?ChurchOfTheLeastFixedPoint=),
an obscure mystical cult.]

May all who follow [the way of letters and lines](https://en.wikipedia.org/wiki/Type_theory) find the wisdom they seek.
It is [the ultimate imperative](https://www.researchgate.net/publication/37596655_Lambda_The_Ultimate_Imperative) to seek wisdom,
to debunk myths, to build [towers](https://en.wikipedia.org/wiki/Numerical_tower),
to [speak hesitatingly](https://en.wikipedia.org/wiki/Lisp_(programming_language).

Those who value [purity](https://en.wikipedia.org/wiki/Haskell), those who breed [desert beasts of burden](https://en.wikipedia.org/wiki/OCaml),
[those who calculate, and perhaps even those who scheme](https://www.cs.kent.ac.uk/people/staff/dat/miranda/wadler87.pdf),
they may argue whether they [should be pure or impure](https://homepages.inf.ed.ac.uk/wadler/papers/marktoberdorf/baastad.pdf),
but they all know that all analogies are false.
Analogies that involve [flatbread wraps](https://byorgey.wordpress.com/2009/01/12/abstraction-intuition-and-the-monad-tutorial-fallacy/) are especially fallacious,
but other analogies are only marginally, if at all, better.

But then those who [breed fowl](https://en.wikipedia.org/wiki/Coq), claim that if something is not true,
you [should not infer that the opposite is true](https://en.wikipedia.org/wiki/Intuitionistic_logic).
They allowed themselves to become [dependent](https://en.wikipedia.org/wiki/Dependent_type) on that,
and their commitment is [total](https://en.wikipedia.org/wiki/Total_functional_programming).

You should not, however, fear reaching the bottom. The [bottom](https://en.wikipedia.org/wiki/Bottom_type) is uninhabited,
and you may find solace in that [fast and loose reasoning is morally correct](https://www.researchgate.net/publication/220997017_Fast_and_loose_reasoning_is_morally_correct).
