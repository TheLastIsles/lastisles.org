# Calendar

Keeping track of time and day is important for everyone. Farmers need to know when to sow the seeds,
priests need to know when to perform rituals, merchants need to plan their expenses and so on.
One who doesn't understand the calendar is navigating the time blind.

The longest known cycle is the year. Many things change during the year: weather, day length,
the position of the Sun at dawn, and visibility of constellations.
The full cycle of star visibility change, as they rise higher over the horizon or hide under it,
take 377 days. This number is divisible by 29, which gives us 13 months, each 29 days long.
Conveniently, this is almost equal to the period between a new moon and a full moon,
which was the basis for old calendars.

Every month is divided into four weeks of seven days each and one additional day.
Traditionally, the extra day is reserved for rest, but how strictly it's observed varies.
Nobles and merchants who don't let their workers rest on that day are looked down upon.
