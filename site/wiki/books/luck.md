# On the nature of luck

Many scholars are concerned by the apparent contradiction between the nature of luck
and the assumption that the world is balanced.

Indeed, the god of luck can bless his followers with greater luck: their actions become consistently more successful.
Existence and effectiveness of amulets and spells is also indisputable.

Some people claim that when you increase your luck right now, you are simply borrowing luck
from your future self. This is easy to disprove. People blessed by the god of luck
and posessors of powerful amulets tend to live long and happy lives,
unless they test their luck incessantly and take a lot of risks.

Others claim that you are borrowing luck from someone else. That is more plausible, but still likely wrong.
More people worshipping the god of luck never seems to create more jinxed people.

When two people equally blessed by the god of luck meet, the blessing seems to protect them from the most unfavorable outcomes.
For example, when two such people toss a coin multiple times, they usually win and lose equally often so that no one is a clear winner.

This gave rise to the idea that a blessing does not cancel bad luck,
but distributes it over a longer time period. This theory is hard to test, but definitely intriguing.
One way to test it would be to find people who got protected from great dangers
by a blessing or an amulet, and observe if fortunate events happen to them less often after that.

Yet there's another theory that I think is even more intriguing and elegant,
even if completely impossible to prove or disprove.
Its claim is that there's not one world, but infinitely many different worlds.
Any single world may be unbalanced by itself, but there's a world that differs from
it in a way to make the entire universe balanced.

Whenever an event with more than one possible outcome occurs, they all happen at once.
Suppose you toss a coin. Now there are two unconnected worlds that differ in one detail:
which side your coin fell on. Since that point they keep diverging.

If that is true, then there is no moral problem with trying to improve your luck in your world,
but there's also no way to truly improve it in the universe as a whole.
