# Penal code

To prevent criminals from fleeing to seek a lighter sentence elsewhere,
all counties of Engwelia and all settlements of the K'Tsonga lands agreed upon
a common penal code.

## Crimes against honor

Verbal insult is punished by a fine of 50 coins and the offender must make a private apology.

Slander is punished by a fine of at least 100 coins and the offender must make a public apology.

## Crimes against property

Tresspassing, that is, entering someone else's property without breaking into it
is punished by a fine of 50 coins or a week of imprisonment.

Burglary, that is, entering someone else's property by breaking into it 
is punished by a fine of 200 coins or a month of imprisonment.

Larceny, that is, taking someone else's possession without owners's consent
when the property value doesn't exceed 1000 coins is punished by a fine of 200 coins
plus the value of stolen property, or a month of imprisonment for every 200 coins
of stolen property value.

Grand larceny that involves property worth more than 1000 coins is punished
by a fine of 500 coins plus the value of stolen property, or by a month of imprisonment
for every 200 coins of stolen property value.


