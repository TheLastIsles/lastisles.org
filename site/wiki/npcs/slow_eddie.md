# Slow Eddie

Slow Eddie is a cognitively impaired <lore>Engwelian</lore> living in his appartment on the second floor
above the <place>Blade and Shield</place> shop. He largely relies on the shop proprietor's charity
and sometimes does menial work for him.

He has no memory of his past but claims he was a rising star until his superiors put a stop
to his career.

He is a reference to [Fast Eddie](https://en.uesp.net/wiki/Morrowind:Edd_%22Fast_Eddie%22_Theman)
from The Elder Scrolls III: Morrowind.

If you talk to the proprietor about him, he will say that he found Eddie in the wilderness,
"and there he was, alone, naked". That's also a reference to one of the possible NPC phrases
in Morrowind.

## Dialog

Most of what he says is just nonsense of oblique references to Morrowind (or both).

* "Oh... Yes... Hello. Under sun and sky..."
* "Soul sickness. That's right. That's that I got. My soul hurts!"
* "That old man with a sweet tooth. Where is he? Where am I?"
* "Mike is a liar! He's a liar, I tell you!"
* "The river. Three bridges. South wall. Never forget. Cannot forget."
* "She's all out of eggs. Empty shells everywhere."
* "Gods, have mercy! Bitter mercy! Only bitter mercy for me now."
* "Read my lips. I can be your mouth. Give me a staff, and I will be your mouth."

However, some of them are true facts stated in a cryptic fashion:

* "Demons fear your fists. I can feel their fear."
* 
