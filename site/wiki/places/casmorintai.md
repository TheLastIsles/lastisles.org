# Casmorintai

Casmorintai is a county on the Northern Isle. Its name means "home of red stones" in the <lore>Gaedelech language</lore>.
That name comes from the abudance of ferrous rock and soil in that region.

Its capital city is <place>Lentaiclas</place>.

It has little arable land and historically was the poorest county in <place>Engwelia</place>.
In the last decades, however, it became a large exporter of technology and crafts, thanks to efforts
of countess <npc>Sheela Dinsarni</npc>, an inventor and a patron of scholars and artisans.

One of her most profitable inventions is the naval fire — a weapon for destroying enemy ships.
It's a mix of substances in a sealed pot that spontaneously ignites when the pot is broken,
burns in water, and the fire is difficult to put out.
Its composition is a closely-guarded secret and it's not even sold — rather, county's navy
provides escort to merchant ships for a fee.
