# Lost Assassin

If you are in the Assassins Gang, the leader will tell you about a member
missing in action:

```
$assassinName specialized in staging accidents and making people disappear without a trace.
Ironically, now he has disappeared without a trace himself, together with his mark.
Since the mark disappeared, the contract is fulfilled, but I wonder about his fate.
He was a valuable asset: skilled and focused, completely ignored all pleasures of life
like wine and woman.
His last contract was to make a man from $town disappear, his wife $wifeName placed it.
```

If you go to $town and talk to $wifeName, she will tell you:

```
He went hunting two months ago and never came back. I thought he was finally on a way
to becoming a real man when he went hunting first time in his life, but it was too much for
such a pathetic excuse for a husband I guess, probably for killed by his own prey.
```

She'll never tell you anything else, so you are completely on your own.
Ask people around $town and they will tell you that he most likely went to the shrubland
to the east of $village.

```
Went hunting and never came back? Probably to the Black Horn peninsula.
It's an impenetrable shrubland and at least one clueless "great hunter"  gets lost there every year.
```

If you ask the proprietor of a public house on the road to the Black Horn, she will tell you
that she remembers two people getting supplies from her and heading to the Black Horn
whom she doesn't remember coming back.

That's the last clue you will get, however. But if you keep searching the Black Horn,
you will eventually find a log cabin inhabited by two men, one of them named $assassinName.

He will tell you the following story:

```
Yes, I was given a contract to make $husbandName disappear. My plan was to gain his trust,
invite him to a hunting trip, and make sure that he has a fatal accident during that trip.
However, when I met him, I was just charmed... I fell in love with him.
And then I discovered that the feeling is mutual.
Since I couldn't return without fulfilling my contract, and he thought if his wife hired assassins
she will eventually get him killed one way or another anyway, we decided to start a new life here in the wilderness.
It's rough here, but at least we are together and no one will find us here.
Do you promise to keep our secret?
```

You can refuse, in which case they both will attack. The assassin is a professional, so prepare for a tough fight.
Once both are dead, you can return to the assassin gang leader for a reward.

If you decide not to rat them out, the assassin will teach you some skills immediately
and offer training at a greatly reduced cost afterwards.
