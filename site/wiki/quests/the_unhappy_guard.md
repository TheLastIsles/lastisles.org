# The unhappy guard

In $town you may meet a guard named $name who seems to be a deeply unhappy and disturbed person.
He suspects that his wife is cheating and wants you to help him find out the truth.

Ask people about him and you'll hear that some indeed saw someone visit his wife lately.

If you search his house, you will find an amulet in the drawers. You may confront his wife about it
and she will admit that it was a gift from an admirer, but tells you that she plans to sell it
to get more money for the family.

She will also tell you that her husband seems close to a mental breakdown from constant abuse
by the guard captain and ask you to do something about it.

You can refuse to return the amulet to her and take it to $guard as evidence.
If you do, within the next day he will stab his wife to death and then drown in the nearby lake while trying to get rid
of that knife. The guard captain will thank you for "arranging such a good show"
but regret that he will find someone else to entertain him now.

If you decide to help her, ask other guards about the captain. They will tell you that he's indeed
quite sadistic and often abuses his troops for amusement and makes them do menial work
such as shaving him.

You can then report the captain of $town to the guard captain in the capital. The town captain
will then be demoted to a town janitor, while $guard will be promoted to the rank of sergeant.

$guard will thank you for your help and the disposition of all guards of $town will rise by 30 points.
Except for the former captain, of course, his disposition will drop by 30 points instead.

## Notes

The guard is a reference to <wikipedia>Wozzeck</wikipedia> from Alban Berg's opera by the same name.
