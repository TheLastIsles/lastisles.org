# Blacksmith's wedding ring

If you visit the blacksmith in $town, he will ask if you can do him a favor.

```
Maybe you can do me a little favor. When I was young, my bride and I could barely afford a marriage ceremony, much less a wedding ring.
Years later when I was making more money, I wanted to make up for it. Twenty years ago I bought her a ring
and took her for a trip to her childhood hometown. Unfortunately, she caught some kind of fever on the way.
I didn't have a potion with me, and by the time we reached $town, it was too late already.
I had to pawn that very ring to pay for her burial. It was such a fancy ring!
She saw such rings on many newlyweds and wanted one for herself. Oddly, I barely remember what it looked like.
I remember my wife's happy face when I gave her the ring, but not the ring itself.
I know it's probably hopeless, but if you go to $town, could you ask pawnbrokers about that ring?
There's no way to bring my wife back, but maybe a little miracle is possible and I can have something to remember her by.
```

If you go to $town, you will find two pawn shops there. One pawnbroker will tell you that he opened his shop just five years ago
and it couldn't possible be his shop.

The other pawnbroker, if his disposition is high enough (over 60), he will tell you the following:

```
Panwed his dead wife's ring twenty years ago, doesn't remember what it looked likem, but wants it back? Are you sure that man is in his right mind?
Do you know how many wedding rings I bought from people and how many sob stories I heard in those twenty years?
Anyway, now that I think of it... there was a fad for a certain kind of wedding rings about twenty years ago.
Those with multiple gems arranged in a circle. Maybe her ring was one of those.
I think I have one in that dusty box... you can have it for fifty coins if you want.
```

Take that ring back to the blacksmith. He will think it's indeed his late wife's ring and reward you with 200 gold.
