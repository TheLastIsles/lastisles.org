# Unfriendly rivalry

If you go to $town and ask about people, the locals  will tell you the following about $merchant:

```
He's a wine merchant. Doesn't run his own shop anymore, only sells wine by the crate to smaller merchants.
Exactly the kind of a merchant everyone hates. $anotherMerchant, his biggest competitor was beaten to death by bandits a while ago.
Guess what he did in response? Raised his prices even higher.
He only thinks about profits and has no compassion.
```

If you talk to the merchant and ask him about jobs, he will offer you to do some dirty work for him:

```
$anotherMerchant wasn't really an honest merchant. He was selling his wine too cheap, as if to undermine me.
I warned him, but he continued to sell his under-priced goods. Then I hired a few men to deal with him.
I only wanted them to talk some sense into him, nothing more. They did their job too well, though.
Hit him in the head with a club, he collapsed and died.
I would say good riddance, but those men know too much now, and they are just town drunkards:
they aren't good at hiding, and if the guards find them, they surely will confess and point at me
in exchange for a ligher sentence. Professional assassins wouldn't rat their customer out
since they are bound by an honor code... or so I heard.

Anyway, could you find those men and deal with them? I wouldn't want to hire professional assassins to deal with them...
I mean, how would I even know how to hire professional assassins? I'll pay you well if you help me.
```

You can reject his offer. He will be disappointed and his disposition will drop by 30 points.
You can also report him and his henchmen to the guards.

If you agree to help him, he will give you a bottle of wine labeled with the name of his dead competitor
and explain that the wine is poisoned.
Bring that bottle to his henchmens' leader in the abandoned house just north of the city.

If you visit that house a day later, you will find three dead bodies there. Return to the wine merchant
and receive your reward: 300 coins and a bottle of "$otherMerchant's Finest Wine".

Just make sure not to drink that wine: it's as poisoned as the first bottle and will easily kill any
low or mid-level character.

## Notes

The effects of the poisoned wine are "damage health" and "blind", hinting that the wine merchant poisoned
it with <wikipedia>methanol</wikipedia>.
