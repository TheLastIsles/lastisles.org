The Last Isles is a fantasy RPG project.

The idea is to revive the spirit of (Western) computer RPGs from the '90s and early '00s:

* Detailed, carefully crafted world and lore.
* Meaningful choices and role-playing options.
* Non-linear gameplay.
* Interwoven quest lines and conflicting factions.

In particular, it takes inspiration from The Elder Scrolls (<wikipedia>Daggerfall</wikipedia> and <wikipedia>Morrowind</wikipedia>),
<wikipedia page="Fallout (video game)">Fallout</wikipedia>, <wikipedia page="Arcanum:_Of_Steamworks_and_Magick_Obscura">Arcanum</wikipedia>,
and <wikipedia page="Gothic (series)">Gothic</wikipedia>.

## The blurb

<blockquote>
You are washed ashore near the capital of <place>Engwelia</place>, unconscious, and discovered by city guards.
They introduce you to the king, who is looking for people to send on a quest to recover a relic
that was lost years ago. He thinks if you survived that, you must be lucky enough to also
succeed at his task. You are neither the first nor the only one on that quest, and some openly
doubt its importance. You can put all your effort in that quest... or simply promise to do it
and go on about your business. The choice is yours.
</blockquote>

## Project status

The game will be released when it's ready. Right now it's in the early design and planning stage.
You are welcome to [contribute](/contribute) to it!

To see already existing material, you should browse the [wiki](/wiki). During development, that wiki will serve
as a design document storage and coordination tool; after the game is released it will serve as a guide for players
(after all, a quest design document _is_ also its walkthrough).

To learn about the setting, check out the [lore section](/wiki/lore).

## Intro theme — "Call to Adventure"

<youtube title="Call to Adventure" border=1>9F7wMTeP-iE</youtube>

## Technical information

* The game will be built with the [OpenMW](https://openmw.org) engine.
* It will natively support Linux, Microsoft Windows, and macOS (possibly Android, too).<fn id="cross-platform">OpenMW or its forks already support all those platforms so it's not an overly ambitious goal.</fn>
* Single-player by default, co-op multiplayer may be added at some point (based on the [TES3MP project](https://tes3mp.com/)).
* Hardware requirements are going to be low to moderate.
* Game world and assets will be under a <wikipedia page="Free-culture movement">free</wikipedia>, strong <wikipedia>copyleft</wikipedia> license ([CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)).

## What will NOT be in the game

### Voiced dialog

Wouldn't be in the game even if there was an unlimited budget. Fully voiced dialog limits the amount
of dialog in the game (since drive space is finite).
Even more importantly, it prevents modders from adding more content to the game.
Well, they can add dialog, but it will not blend in with the rest of the game unless they can hire
the same voice actors.

Absence of voiced dialog is a compromise necessary to make the game infinitely extensible.

### Enemy and loot scaling (aka the "levelling problem")

As in, when your character is low-level, you will not be able to do heroic deeds that no one else could do.
If you like to enter random locations to look for trouble, you should always be prepared to run for your life
and return later.

However, old quests will not become harder as your character grows, and you will never be locked out
of any loot. If you are skilled and creative enough to defeat or bypass enemies guarding a powerful artifact,
you can get that artifact at any level.

### Procedurally-generated content

Outside of rogue-likes, it rarely goes well.

### Other things that will not be in the game

#### No "hero saving the world" plot.

The world certainly has its problems, but it's not in any immediate
danger of destruction and there's no Great Evil™ to fight. Ignoring the main quest is a valid choice both
gameplay-wise and story-wise.<fn id="daggerfall">The story of The Elder Scrolls II: Daggerfall was a major inspiration in this regard.</fn>

#### No typical fantasy races. No elves, dwarves, or orcs.

There are no "racial abilities" either: humans are humans with different appearance and that's about it.
