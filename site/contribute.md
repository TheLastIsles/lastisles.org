# Contributing

The Last Isles is now in a very early stage of design and planning.

Here is the plan:

1. Create a detailed, internally-consistent world. That work can be used as an elaborate tabletop RPG setting by itself.
2. Create game assets (meshes, textures, animations...).
3. Finally, create a playable version based on OpenMW.

Everyone is welcome to join the work! Since everything is under a strong copyleft license (CC-BY-SA),
no one can appropriate the work of contributors for a proprietary commercial project.
