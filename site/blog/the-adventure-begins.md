<h1 id="title">The adventure begins</h1>

<p>Date: <time id="last-modified">2022-08-08</time> </p>
<p>Author: <span id="author">Daniil Baturin</span> </p>

<p id="excerpt">
The Last Isles project has formally began. It almost certainly will not be moving very fast,
but there is no pressure either. The main goal is to make it fun for players when it is ready
and fun for developers while it still is not.
</p>

In reality, the project, then unnamed, began in the summer of 2020. At the time I was thinking
about classic western RPGs a lot: what made them so great, and what I might want from their spiritual
successors, if they are ever made. Eventually I started getting vague idea for a setting
and a plot that I thought were good enough to make an actual game out of them.
Until recently, I only showed my drafts to a limited number of close friends,
but I think now it's time to make it real.

Making a open-world RPG completely from scratch is a completely unrealistic option.
For a long time, independent game of that genre were only made as total conversions
of existing games, such as <wikipedia>Enderal</wikipedia> built on the TES V: Skyrim engine,
or its predecessors from the same team built on the engines of Oblivion and Morrowind.

There are also incredibly impressive world extension projects, such as
[Tamriel Rebuilt](https://openmw.org). Tamriel Rebuilt is very inspiring because it shows
that a community-driven project can product cohesive results on a large scale
if the underlying world is sufficiently detailed.

However, all those projects have a dependency on a proprietary engine. You cannot play Enderal
if you don't buy TES V: Skyrim first. The position of Tamriel Rebuilt is even more precarious:
it can only exist as long as ~~Bethesda~~ ~~Zenimax~~ Microsoft chooses not to send them
a takedown notice.

Can we make something that truly belongs to the community of players and modders?
Technically, we now can: the [OpenMW](https://openmw.org) engine is a free (as in freedom)
and open-source implementation of an open-world RPG engine. It still bears many marks
of its original goal: support for playing TES III: Morrowind without its original engine —
proprietary, outdated, and limited. For that reason it still has a lot of logic specific
to Morrowind, but it had many big breakthroughs in the last year with respect to extensibility,
and dehardcoding that logic is one of its goals.

If the game itself is under a free-culture license, the game itself and its world
will be available to all people to play and build upon. By choosing a _copyleft_ license
that requires modificaions to be under the same license, we can protect it from getting
appropriated: any derivative work must also belong to the community.

If a game is to be open to contributions from everyone and the copyright remains shared
between multiple people, it also means no one, including the de facto project leader,
can profit from it by selling it — everyone who gets it is free to redistribute it,
and no one can change its license to prevent that.

That means the game will not be on any big platforms like Steam. However, not being
on those platforms can also be liberating. There's no need to comply with censorship
imposed by those big players that, for example, classifies a naked butt as 18+ content
while mass murders are perfectly appropriated for a "teen" rating.

