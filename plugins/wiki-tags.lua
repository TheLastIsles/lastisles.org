prefix = "/wiki"

mappings = {}
mappings["lore"] = "lore"
mappings["place"] = "places"
mappings["book"] = "books"
mappings["npc"] = "characters"
mappings["quest"] = "quests"
mappings["development"] = "development"

function process_page_tag(p)
  tag_name = HTML.get_tag_name(p)

  HTML.set_tag_name(p, "a")
  page_name = HTML.get_attribute(p, "page")
  if not page_name then
    page_name = HTML.strip_tags(p)
  end

  page_name = strlower(page_name)
  page_name = Regex.replace(page_name, "\\s+", "_")
  location = mappings[tag_name]
  url = format("%s/%s/%s", prefix, location, page_name)
  HTML.set_attribute(p, "href", url)

  -- Hardcode Markdown extension for now at least,
  -- in practice all pages are in Markdown
  if not Sys.file_exists(format("%s/%s/%s/%s.md", site_dir, prefix, location, page_name)) then
    HTML.add_class(p, "nonexistent-page")
  end

end

page_tags = HTML.select_all_of(page, {"lore", "place"})

Table.iter_values(process_page_tag, page_tags)
