
-- Clunky CSS hack to make the iframe responsive
embed_code_tmpl = [[
  <div style="position: relative; width: 100%%; height: 0; padding-bottom: 56.25%%;">
    <iframe src="https://www.youtube.com/embed/%s"
      style="position: absolute; top: 0; left: 0; width: 100%%; height: 100%%;"
      title="%s"
      frameborder="%s" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowfullscreen>
    </iframe>
  </div>
]]

function make_yt_embed(elem)
  id = HTML.inner_html(elem)
  title = HTML.get_attribute(elem, "title")

  border = HTML.get_attribute(elem, "border")
  if not border then
    border = "1"
  end

  embed_code = format(embed_code_tmpl,
    id, title, border)
  embed_html = HTML.parse(embed_code)

  HTML.replace_element(elem, embed_html)
end

yt_tags = HTML.select(page, "youtube")
Table.iter_values(make_yt_embed, yt_tags)

